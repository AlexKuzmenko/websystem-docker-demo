const mongoose = require("mongoose")
const url = process.env.MONGODB_URL || "mongodb://localhost:27017/api"

module.exports.connectDB = () => {
    mongoose.connect(url, {useNewUrlParser: true})
    return mongoose.connection;
}