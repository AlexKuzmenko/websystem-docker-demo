const express = require('express');
const {connectDB} = require("./utils/db")
const port = process.env.PORT || 3001
const host = process.env.HOST || 'http://localhost'
const {User} = require("./models/user")

// приложение
const app = express();

app.get('/', (req, res) => {
    res.send('World from Docker');
});

app.get("/users", async (req, res) => {
    const user = new User({userName: "Alex"})
    await user.save()
    const users = await User.find()
    res.json(users)
})


const startServer = () => {
    app.listen(port, () => {
        console.log(`Server is running on ${host}:${port}`)
    });
}

connectDB()
    .on("error", (error) => {
        console.log(error)
    })
    .once("open", startServer)

